package irc;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import junit.framework.TestCase;

public class IrcMessageTest extends TestCase {
    @Test
    public void testParse() throws Exception {
        IrcMessage m = IrcMessage.parse(":*.freenode.net NOTICE * :*** Looking up your ident...");
        assertEquals("Incorrect prefix", "*.freenode.net", m.prefix);
        assertEquals("Incorrect command", "NOTICE", m.command);
        assertArrayEquals("Incorrect command", new String[] { "*", ":***", "Looking", "up", "your", "ident..." }, m.parameters);
    }

    @Test
    public void testNick() throws Exception {
        NickIrcMessage m = new NickIrcMessage("Martin");
        assertEquals("Incorrect nick command", "NICK Martin", m.toString());
    }

    @Test
    public void testPingPong() throws Exception {
        IrcMessage m = new IrcMessage(null, "PING", new String[] { "localhost" });
        PongIrcMessage pong = new PongIrcMessage(m);
        assertEquals("Incorrect pong command", "PONG localhost", pong.toString());
    }
}
