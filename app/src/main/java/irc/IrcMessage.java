package irc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IrcMessage implements IrcMessageConstants {
    private static final int IRC_MAX_PARAMETERS = 15;
    private static final String IRC_PREFIX_MARKER = ":";

    String prefix;
    String command;
    String[] parameters = new String[IRC_MAX_PARAMETERS];

    private static final String SPACE = " ";

    private static final Logger logger = LoggerFactory.getLogger(IrcMessage.class);

    public IrcMessage(String prefix, String command, String[] parameters) {
        this.prefix = prefix;
        this.command = command;
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return (prefix == null ? "" : prefix + SPACE) + command + (parameters == null ? "" : SPACE + String.join(SPACE, parameters));
    }

    public static IrcMessage parse(String message) throws RuntimeException {
        String _prefix = null, _command = null;
        String[] _parameters = null;

        // parse message into IrcMessage object
        //
        // BNF representation of a message is: (source: https://datatracker.ietf.org/doc/html/rfc2812#section-2.3.1)
        //
        // message    =  [ ":" prefix SPACE ] command [ params ] crlf
        // prefix     =  servername / ( nickname [ [ "!" user ] "@" host ] )
        // command    =  1*letter / 3digit
        // params     =  *14( SPACE middle ) [ SPACE ":" trailing ]
        //            =/ 14( SPACE middle ) [ SPACE [ ":" ] trailing ]

        // nospcrlfcl =  %x01-09 / %x0B-0C / %x0E-1F / %x21-39 / %x3B-FF
        //                 ; any octet except NUL, CR, LF, " " and ":"
        // middle     =  nospcrlfcl *( ":" / nospcrlfcl )
        // trailing   =  *( ":" / " " / nospcrlfcl )

        // SPACE      =  %x20        ; space character
        // crlf       =  %x0D %x0A   ; "carriage return" "linefeed"

        // sample message:
        // :*.freenode.net NOTICE * :*** Looking up your ident...

        // parse message
        int idx = 0;
        
        // parse prefix
        if (message.startsWith(IRC_PREFIX_MARKER)) {
            _prefix = message.substring(1).split(SPACE)[0];
            idx = message.indexOf(SPACE)+1;
        }

        // parse command
        String[] commandAndParams = message.substring(idx).split(SPACE);
        _command = commandAndParams[0];
        if (commandAndParams.length > 1) {
            // parse parameters
            idx += message.substring(idx).indexOf(SPACE)+1;
            String params = message.substring(idx);
            // logger.debug("Params: " + params);
            _parameters = params.split(SPACE);
        }

        return new IrcMessage(_prefix, _command, _parameters);
    }
    
}
