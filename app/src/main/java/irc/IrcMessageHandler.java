package irc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handle an incoming IRC message.
 */
public class IrcMessageHandler implements IrcMessageConstants {

    private static final Logger logger = LoggerFactory.getLogger(IrcMessageHandler.class);
    public String process(IrcMessage message) {
        String result = null;
        // logger.debug(message.toString());

        switch(message.command) {
            case NOTICE_COMMAND:
            case RPL_WELCOME:
            case RPL_YOURHOST:
            case RPL_CREATED:
            case RPL_MYINFO:
            case RPL_BOUNCE:
            case RPL_MOTDSTART:
            case RPL_MOTD:
            case RPL_MOTDEND:
            case RPL_LUSERCLIENT:
            case RPL_LUSEROP:
            case RPL_LUSERUNKNOWN:
            case RPL_LUSERCHANNELS:
            case RPL_LUSERME:
            case RPL_UNKNOWN1:
            case RPL_UNKNOWN2:
            case RPL_UNKNOWN3:
            case MODE_COMMAND:
                break;
            case PING_COMMAND:
                result = new PongIrcMessage(message).toString();
                break;
            default:
                logger.error("Unknown command " + message.command);
                break;
        }
        return result;
    }

}
