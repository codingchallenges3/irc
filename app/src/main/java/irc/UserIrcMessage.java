package irc;

public class UserIrcMessage extends IrcMessage {

    public UserIrcMessage(String user, String mode, String realname) {
        super(null, USER_COMMAND, new String[] { user, mode, "*", realname});
    }
    
}
