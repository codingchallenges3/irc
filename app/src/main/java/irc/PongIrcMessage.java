package irc;

public class PongIrcMessage extends IrcMessage {

    public PongIrcMessage(IrcMessage message) {
        super(null, PONG_COMMAND, new String[] { message.parameters[0] });
    }

}
