package irc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;
import io.netty.util.CharsetUtil;

/**
 * Handles a server-side channel.
 */
public class IrcClientHandler extends ChannelInboundHandlerAdapter { // (1)

    private static final Logger logger = LoggerFactory.getLogger(IrcClientHandler.class);
    private final AttributeKey<StringBuffer> DATA_KEY = AttributeKey.valueOf("dataBuf");

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) { // (2)
        ByteBuf byteBuf;
        String data;

        Channel channel = ctx.channel();
        StringBuffer dataBuf = channel.attr(DATA_KEY).get();
        boolean allocBuf = dataBuf == null;
        if (allocBuf) dataBuf = new StringBuffer();

        byteBuf = (ByteBuf) msg;
        data = byteBuf.toString(CharsetUtil.UTF_8);

        if (!data.isEmpty()) {
            // logger.debug(String.format("Data received %s from %s", data, hostAddress));
        }
        else {
            // logger.debug(String.format("NO Data received from %s", hostAddress));
        }

        dataBuf.append(data);
        if (allocBuf) channel.attr(DATA_KEY).set(dataBuf);

        channel.read();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        StringBuffer dataBuf = channel.attr(DATA_KEY).get();

        // ensure that the data buffer ends with an "\n" so that we have one or more complete commands
        // without this check sometimes the data buffer ends in the middle of a command
        if (dataBuf != null && dataBuf.toString().endsWith("\n")) {
            String reply = null;

            String[] messages = dataBuf.toString().split("\n");
            for (String message : messages) {
                logger.debug("RECEIVED: " + message);
                reply = new IrcMessageHandler().process(IrcMessage.parse(message));

                if (reply != null) {
                    logger.debug("SENDING: " + reply);
                    if (! reply.endsWith("\n")) {
                        reply = reply + "\n";
                    }
                    ctx.write(Unpooled.copiedBuffer(reply, CharsetUtil.UTF_8));
                }
            }

            // only when the data was processed and sent do we clear out the buffer
            channel.attr(DATA_KEY).set(null);
            ctx.writeAndFlush(Unpooled.EMPTY_BUFFER); // .addListener(ChannelFutureListener.CLOSE);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        // Close the connection when an exception is raised.
        logger.debug("Exception caught: " + cause);
        cause.printStackTrace();
        ctx.close();
    }
}