package irc;

public interface IrcMessageConstants {
    String RPL_WELCOME = "001";
    String RPL_YOURHOST = "002";
    String RPL_CREATED = "003";
    String RPL_MYINFO = "004";
    String RPL_BOUNCE = "005";
    String RPL_MOTDSTART = "375";
    String RPL_MOTD = "372";
    String RPL_MOTDEND = "376";
    String RPL_LUSERCLIENT = "251";
    String RPL_LUSEROP = "252";
    String RPL_LUSERUNKNOWN = "253";
    String RPL_LUSERCHANNELS = "254";
    String RPL_LUSERME = "255";
    String RPL_UNKNOWN1 = "396"; // not found in the RFC1123
    String RPL_UNKNOWN2 = "265"; // not found in the RFC1123
    String RPL_UNKNOWN3 = "266"; // not found in the RFC1123

    String NOTICE_COMMAND = "NOTICE";
    String PING_COMMAND = "PING";
    String PONG_COMMAND = "PONG";
    String NICK_COMMAND = "NICK";
    String MODE_COMMAND = "MODE";
    String USER_COMMAND = "USER";
}
