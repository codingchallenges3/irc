package irc;

public class NickIrcMessage extends IrcMessage {

    public NickIrcMessage(String name) {
        super(null, NICK_COMMAND, new String[] { name });
    }
    
}
